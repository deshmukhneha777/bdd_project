$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/hp/Desktop/Neha/bdd_cucumber_task/src/test/java/com/feature/files/LoginDataTable.Feature");
formatter.feature({
  "line": 2,
  "name": "Login Using Data Table",
  "description": "",
  "id": "login-using-data-table",
  "keyword": "Feature"
});
formatter.before({
  "duration": 4215287100,
  "status": "passed"
});
formatter.scenario({
  "line": 4,
  "name": "Login to Site",
  "description": "",
  "id": "login-using-data-table;login-to-site",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 3,
      "name": "@SmokeTest"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "I am on a login page",
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "I enter valid data on the page",
  "rows": [
    {
      "cells": [
        "username",
        "priyanka.more"
      ],
      "line": 8
    },
    {
      "cells": [
        "password",
        "Ride12345#"
      ],
      "line": 9
    }
  ],
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "The user login should be successful",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginDataTable.I_am_on_a_login_page()"
});
formatter.result({
  "duration": 46926464000,
  "status": "passed"
});
formatter.match({
  "location": "LoginDataTable.I_enter_valid_data_on_the_page(DataTable)"
});
formatter.result({
  "duration": 916140400,
  "status": "passed"
});
formatter.match({
  "location": "LoginDataTable.The_user_login_should_be_successful()"
});
formatter.result({
  "duration": 6794200,
  "status": "passed"
});
formatter.after({
  "duration": 102655100,
  "status": "passed"
});
formatter.scenarioOutline({
  "comments": [
    {
      "line": 13,
      "value": "#Feature: Infostretch to Login"
    }
  ],
  "line": 15,
  "name": "Login to Infostretch Site",
  "description": "",
  "id": "login-using-data-table;login-to-infostretch-site",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 16,
  "name": "User is on login page of Infostretch",
  "keyword": "Given "
});
formatter.step({
  "line": 17,
  "name": "Title of web page is Infostretch",
  "keyword": "When "
});
formatter.step({
  "line": 18,
  "name": "I enter Username as \"\u003cusername\u003e\" and Password as \"\u003cpassword\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "login should be unsuccessful",
  "keyword": "Then "
});
formatter.examples({
  "line": 21,
  "name": "",
  "description": "",
  "id": "login-using-data-table;login-to-infostretch-site;",
  "rows": [
    {
      "cells": [
        "username",
        "password"
      ],
      "line": 23,
      "id": "login-using-data-table;login-to-infostretch-site;;1"
    },
    {
      "cells": [
        "username1",
        "password1"
      ],
      "line": 24,
      "id": "login-using-data-table;login-to-infostretch-site;;2"
    },
    {
      "cells": [
        "username2",
        "password2"
      ],
      "line": 25,
      "id": "login-using-data-table;login-to-infostretch-site;;3"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 3079995500,
  "status": "passed"
});
formatter.scenario({
  "line": 24,
  "name": "Login to Infostretch Site",
  "description": "",
  "id": "login-using-data-table;login-to-infostretch-site;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 16,
  "name": "User is on login page of Infostretch",
  "keyword": "Given "
});
formatter.step({
  "line": 17,
  "name": "Title of web page is Infostretch",
  "keyword": "When "
});
formatter.step({
  "line": 18,
  "name": "I enter Username as \"username1\" and Password as \"password1\"",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "login should be unsuccessful",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginToInfostretch.user_is_on_login_page_of_Infostretch()"
});
formatter.result({
  "duration": 66926019900,
  "status": "passed"
});
formatter.match({
  "location": "LoginToInfostretch.title_of_web_page_is_Infostretch()"
});
formatter.result({
  "duration": 125942900,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "username1",
      "offset": 21
    },
    {
      "val": "password1",
      "offset": 49
    }
  ],
  "location": "LoginToInfostretch.I_enter_Username_as_and_Password_as(String,String)"
});
formatter.result({
  "duration": 669496700,
  "status": "passed"
});
formatter.match({
  "location": "LoginToInfostretch.login_should_be_unsuccessful()"
});
formatter.result({
  "duration": 101492900,
  "status": "passed"
});
formatter.after({
  "duration": 94010300,
  "status": "passed"
});
formatter.before({
  "duration": 3115707300,
  "status": "passed"
});
formatter.scenario({
  "line": 25,
  "name": "Login to Infostretch Site",
  "description": "",
  "id": "login-using-data-table;login-to-infostretch-site;;3",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 16,
  "name": "User is on login page of Infostretch",
  "keyword": "Given "
});
formatter.step({
  "line": 17,
  "name": "Title of web page is Infostretch",
  "keyword": "When "
});
formatter.step({
  "line": 18,
  "name": "I enter Username as \"username2\" and Password as \"password2\"",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "login should be unsuccessful",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginToInfostretch.user_is_on_login_page_of_Infostretch()"
});
formatter.result({
  "duration": 67026896900,
  "status": "passed"
});
formatter.match({
  "location": "LoginToInfostretch.title_of_web_page_is_Infostretch()"
});
formatter.result({
  "duration": 109249300,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "username2",
      "offset": 21
    },
    {
      "val": "password2",
      "offset": 49
    }
  ],
  "location": "LoginToInfostretch.I_enter_Username_as_and_Password_as(String,String)"
});
formatter.result({
  "duration": 780115000,
  "status": "passed"
});
formatter.match({
  "location": "LoginToInfostretch.login_should_be_unsuccessful()"
});
formatter.result({
  "duration": 116873200,
  "status": "passed"
});
formatter.after({
  "duration": 80566300,
  "status": "passed"
});
});