package com.stepdefinition;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;

public class Login1 {

	WebDriver driver=null;

	@Given("^User is on login page$")
	public void user_is_on_login_page() {

		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\hp\\Desktop\\Neha\\bdd_cucumber_task\\src\\test\\resources\\drivers\\chromedriver1.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://nest.infostretch.com/");
	}

	@When("^Title of web page is Login$")
	public void title_of_web_page_is_Login() {
		String title = driver.getTitle();
		System.out.println(title);
		Assert.assertEquals("Infostretch NEST", title);
	}

	@When("^user enters username and password$")
	public void user_enters_username_and_password() {
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//input[@id='username']")).sendKeys("neha.deshmukh");
		driver.findElement(By.xpath("//input[@id='password']")).sendKeys("Imwatim@4777");
	}

	@When("^user clicks  on login button$")
	public void user_clicks_on_login_button() {
		driver.findElement(By.xpath("//button[@type='submit']")).click();
	}

	@Then("^user is on home page$")
	public void user_is_on_home_page() {
		String title = driver.getTitle();
		System.out.println(title);
		driver.quit();
	}

}
